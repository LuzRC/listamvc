
Simple todo MVC application in plain JavaScript

Learn the MVC pattern by building a small app!

- **Model** - manages the data of an application
- **View** - a visual representation of the model
- **Controller** - links the user and the system
